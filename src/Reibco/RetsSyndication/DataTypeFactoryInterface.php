<?php
namespace Reibco\RetsSyndication;

interface DataTypeFactoryInterface
{

    /**
     * Make a new data object using the given type and parameters.
     *
     * @param string $type
     * @param array $parameters
     * @return mixed
     */
    public function make($type, array $parameters = array());
}
