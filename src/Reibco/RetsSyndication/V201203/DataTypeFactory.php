<?php
namespace Reibco\RetsSyndication\V201203;

use Reibco\RetsSyndication\DataTypeFactoryInterface;

class DataTypeFactory implements DataTypeFactoryInterface
{

    /**
     * Make a new data object using the given type and parameters.
     *
     * @param string $type
     * @param array $parameters
     * @return mixed
     */
    public function make($type, array $parameters = array())
    {
        // build the class name for the object
        $class = 'Reibco\RetsSyndication\V201203\DataTypes\\' . $type;

        if (empty($parameters)) {
            // if no parameters, just create a new instance
            $instance = new $class();
        } else {
            // if parameters, use the reflection class to make a new object
            // with the parameters
            $reflection = new \ReflectionClass($class);
            $instance = $reflection->newInstanceArgs($parameters);
        }

        // return the new object
        return $instance;
    }

    /**
     * Use __call to create a dynamic set of make() methods.
     *
     * Anything after the "make" will be interpreted as the type of object to
     * make. For example, a call makeOriginalEnum() will make an OriginalEnum
     * object.
     *
     * @param string $name
     * @param string $parameters
     * @throws \BadMethodCallException
     * @return mixed
     */
    public function __call($name, $parameters)
    {
        // if the attempted method doesn't begin with 'make', throw an error
        if (strpos($name, 'make') !== 0) {
            throw new \BadMethodCallException('Function ' . $name . ' does not exist.');
        }

        // parse out the type to make
        $type = substr($name, 4);

        // return the made object
        return $this->make($type, $parameters);
    }
}
