<?php
namespace Reibco\RetsSyndication\V201203\DataTypes;

class OriginalEnum extends Enum
{
    protected $original;

    /**
     * Create a new OriginalEnum object.
     *
     * @param string $standard
     * @param string $original
     */
    public function __construct($standard, $original)
    {
        // initialize with the parent constructor
        parent::__construct($standard);

        // set the child data
        $this->original = $original;
    }

    /**
     * Get the OriginalEnum original value.
     *
     * @return string
     */
    public function getOriginalValue()
    {
        return $this->original;
    }
}
