<?php
namespace Reibco\RetsSyndication\V201203\DataTypes;

class Enum
{
    protected $standard;

    /**
     * Create a new Enum object with the given value.
     *
     * @param string $standard
     */
    public function __construct($standard)
    {
        $this->standard = $standard;
    }

    /**
     * Get the Enum value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->standard;
    }
}
