<?php
namespace Reibco\RetsSyndication\V201203\DataTypes;

class Money
{
    protected $value;
    protected $currencyCode;

    /**
     * Create a new Money object with the given value and currency code.
     *
     * @param float $value
     * @param Enum $currencyCode
     */
    public function __construct($value, Enum $currencyCode = null)
    {
        $this->value = $value;
        $this->currencyCode = $currencyCode;
    }

    /**
     * Get the Money value.
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get the Money currency code Enum.
     *
     * @return Enum
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }
}
