<?php
namespace Reibco\RetsSyndication\V201203\DataTypes;

class SecureMoney extends Money
{
    protected $securityClass;

    /**
     * Create a new SecureMoney object with the given value and security.
     *
     * @param float $value
     * @param Enum $securityClass
     * @param Enum $currencyCode
     */
    public function __construct($value, Enum $securityClass, Enum $currencyCode = null)
    {
        // initialize with the parent constructor
        parent::__construct($value, $currencyCode);

        // set the child data
        $this->securityClass = $securityClass;
    }

    /**
     * Get the SecureMoney security class Enum.
     *
     * @return Enum
     */
    public function getSecurityClass()
    {
        return $this->securityClass;
    }
}
