<?php
namespace Reibco\RetsSyndication\V201203\DataTypes;

class SecureMoneyFrequency extends SecureMoney
{
    protected $currencyPeriod;

    /**
     * Create a new SecureMoneyFrequency object with the given value, security,
     * and frequency.
     *
     * @param float $value
     * @param Enum $securityClass
     * @param Enum $currencyPeriod
     * @param Enum $currencyCode
     */
    public function __construct($value, Enum $securityClass, Enum $currencyPeriod = null, Enum $currencyCode = null)
    {
        // initialize with the parent constructor
        parent::__construct($value, $securityClass, $currencyCode);

        // set the child data
        $this->currencyPeriod = $currencyPeriod;
    }

    /**
     * Get the SecureMoney currency period Enum.
     *
     * @return Enum
     */
    public function getCurrencyPeriod()
    {
        return $this->currencyPeriod;
    }
}
